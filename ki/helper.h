#ifndef CONSTI_KI_HELPER
#define CONSTI_KI_HELPER

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define FIELD_FREE '*'

typedef struct {
	int row;
	int col;
} Point;

/*
 * The Server uses the alphabet and numbers from 1 upwards for the field
 * Convert row/col from int to char
**/
char convertRow(const Point p);
char convertCol(const Point p);

/*
 * Helper function to debug a point
**/
void printPoint(const Point p);

/*
 * Check if Point is in game field range, e.g. in range 0..8
**/
bool validate(const Point p);

/*
 * Get char from playing field at Position p
**/
char getVal(const Point p,const char playingField[8][8]);

/*
 * Check if the char is an enemy of the player 
 * depending wether he is White or Black
**/
bool isEnemy(const char val,const bool white);



#endif