
#include "heuristic.h"

//Table taken from https://github.com/Timdpr/checkers-human-ai
// /blob/6dc20741c3e4162bf1b90112fe781c95e21db504/src/main/java/model/AI.java#L162

static const int positionWeightLookup[][8] = 
{{0, 4, 0, 4, 0, 4, 0, 4},
 {4, 0, 3, 0, 3, 0, 3, 0},
 {0, 3, 0, 2, 0, 2, 0, 4},
 {4, 0, 2, 0, 1, 0, 3, 0},
 {0, 3, 0, 1, 0, 2, 0, 4},
 {4, 0, 2, 0, 2, 0, 3, 0},
 {0, 3, 0, 3, 0, 3, 0, 4},
 {4, 0, 4, 0, 4, 0, 4, 0}};
 
 
//just count how many pieces are standing.
//1 point for men, 2 points for queen
int heuristic_simple(const char playingField[8][8],const bool white){
	const char youMen=white ? 'w' : 'b';
	const char youQueen=white ? 'W' : 'B';
	int nMen=0;
	int nQueen=0;
	for(int i=0;i<8;i++){
		for(int j=0;j<8;j++){
			if(playingField[i][j]==youMen){
				nMen++;
			}
			if(playingField[i][j]==youQueen){
				nQueen++;
			}
		}
	}
	//if((nMen+nQueen)==0){
	//	return INT_MIN;
	//}
	const int score=nMen+nQueen*2;
	return score;
}


void promotePieces(char playingField[8][8],const bool white){
	const char youMen=white ? 'w' : 'b';
	const char youQueen=white ? 'W' : 'B';
	const int youFarthestRow= white ? 8 : 0;
	for(int i=0;i<8;i++){
		if(playingField[i][youFarthestRow]==youMen){
			playingField[i][youFarthestRow]=youQueen;
		}
	}
}

//uses the loockup table that takes positions into account
int heuristic_advanced(char playingField[8][8],const bool white){
	
	//HACK
	//we did not promote men to queens yet when this function is called
	
	promotePieces(playingField,white);
	
	const char youMen=white ? 'w' : 'b';
	const char youQueen=white ? 'W' : 'B';
	int score=0;
	for(int i=0;i<8;i++){
		for(int j=0;j<8;j++){
			if(playingField[i][j]==youMen){
				const int weight=positionWeightLookup[i][j];
				score+=3*weight;
			}
			if(playingField[i][j]==youQueen){
				const int weight=positionWeightLookup[i][j];
				score+=5*weight;
			}
		}
	}
	return score;
}