#ifndef CONSTI_SIMPLE_KI_H
#define CONSTI_SIMPLE_KI_H

#include <stdbool.h>

#include "table.h"

#define KI_MODE_SIMPLE 0
#define KI_MODE_RANDOM 1
#define KI_MODE_ADVANCED 2


void calculateNewMove(const char playingField[8][8], char* move,const bool white,const int mode);


#endif