
#include "table.h"
#include "heuristic.h"

void printPlayingFieldX(const char playingField[8][8]){
	printf("   A B C D E F G H  \n");
	printf(" +-----------------+ \n");
	for(int i=0;i<8;i++){
		printf("%d|",8-i);
		for(int j=0;j<8;j++){
			printf(" %c",playingField[i][j]);
		}
		printf(" |%d\n",8-i);
	}
	printf(" +-----------------+ \n");
	printf("   A B C D E F G H  \n");
}


void T_Prepare(Table* table){
	kv_init(table->stepMoves);
	kv_init(table->jumpMoves);
	kv_init(table->possibleMoves);
}

void T_addMove(Table* table,TMove* move,const bool isJumpMove){
	if(isJumpMove){
		kv_push(TMove*,table->jumpMoves,move);
	}else{
		kv_push(TMove*,table->stepMoves,move);
	}
	kv_push(TMove*,table->possibleMoves,move);
}

void T_Cleanup(Table* table){
	while(kv_size(table->possibleMoves)>0){
		TMove* move=kv_pop(table->possibleMoves);
		free(move);
	}
	kv_destroy(table->possibleMoves);
	kv_destroy(table->jumpMoves);
	kv_destroy(table->stepMoves);
}

void T_printStepMoves(Table* table){
	const int sizeS=(int)kv_size(table->stepMoves);
	printf("N step Moves: %d\n",sizeS);
	for(int i=0;i<sizeS;i++){
		TMove* move=kv_A(table->stepMoves,i);
		printf("Step Move %d: (steps %d,enemies %d)\n",i,move->stepC,move->enemyC);
		printPoint(move->start);
		printPoint(move->steps[0]);
	}
}

void T_printJumpMoves(Table* table){
	const int sizeJ=(int)kv_size(table->jumpMoves);
	printf("N jump Moves: %d\n",sizeJ);
	for(int i=0;i<sizeJ;i++){
		TMove* move=kv_A(table->jumpMoves,i);
		printf("Jump Move %d: (steps %d,enemies %d)\n",i,move->stepC,move->enemyC);
		printPoint(move->start);
		for(int j=0;j<move->stepC;j++){
			printPoint(move->steps[j]);
		}
	}
}

TMove* TMove_create(){
	TMove* new=malloc(sizeof(TMove));
	new->stepC=0;
	new->enemyC=0;
	return new;
}

TMove* TMove_clone(const TMove* input){
	TMove* new=malloc(sizeof(TMove));
	new->stepC=input->stepC;
	new->enemyC=input->enemyC;
	memcpy(&new->start,&input->start,sizeof(Point));
	memcpy(&new->steps,&input->steps,sizeof(Point)*input->stepC);
	memcpy(&new->enemies,&input->enemies,sizeof(Point)*input->enemyC);
	memcpy(new->playingField,input->playingField,sizeof(char)*64);
	return new;
}

void TMove_setStart(TMove* move,const Point start){
	memcpy(&move->start,&start,sizeof(Point));
}

void TMove_setPlayingField(TMove* move,const char playingField[8][8]){
	memcpy(move->playingField,playingField,sizeof(char)*8*8);
}

void TMove_addStep(TMove* move,const Point step){
	memcpy(&move->steps[move->stepC],&step,sizeof(Point));
	move->stepC++;
}

void TMove_addEnemy(TMove* move,const Point enemy){
	memcpy(&move->enemies[move->enemyC],&enemy,sizeof(Point));
	move->enemyC++;
}

void T_appendJumpMovesRecursive(Table* table,TMove* prev,const bool isQueen);

//This one is fairly simple, since we only have to check for free fields
// and don't have the cascade effect of jump Moves
void T_addStepMovesForPiece(Table* table,const Point pos,const bool isQueen){
	StepMove* stepMoves[isQueen ? 64 : 2];
	int nMoves= isQueen ? stepMovesForQueen(table->playingField,pos,stepMoves) 
						: stepMovesForMen(table->playingField,pos,table->white,stepMoves);
	for(int i=0;i<nMoves;i++){
		StepMove* stepMove=stepMoves[i];
		TMove* move=TMove_create();
		TMove_setPlayingField(move,table->playingField);
		applyStepMove(move->playingField,stepMoves[i]);
		TMove_setStart(move,stepMove->start);
		TMove_addStep(move,stepMove->end);
		free(stepMove);
		T_addMove(table,move,false);
	}
}

//This one is not that easy, since one jump move can have up to X more jump moves
//afterwards, and each later jumpMove might create a new 'branch' (when viewed as an binary tree)
//or a completely new move (when viewed as a list of moves)
void T_addJumpMovesForPiece(Table* table,const Point pos,const bool isQueen){
	JumpMove* jumpMoves[isQueen ? 4 : 2];
	const int nMoves= isQueen ? jumpMovesForQueen(table->playingField,pos,table->white,jumpMoves) 
						: jumpMovesForMen(table->playingField,pos,table->white,jumpMoves);
	//printf("start %d\n",nMoves);
	for(int i=0;i<nMoves;i++){
		JumpMove* jumpMove=jumpMoves[i];
		TMove* new=TMove_create();
		TMove_setPlayingField(new,table->playingField);
		movePieceAndMark(new->playingField,jumpMove);
		TMove_setStart(new,pos);
		TMove_addStep(new,jumpMove->end);
		TMove_addEnemy(new,jumpMove->enemy);
		T_addMove(table,new,true);
		//then append as many jump Moves as possible
		T_appendJumpMovesRecursive(table,new,isQueen);
		removeMark(new->playingField,jumpMove);
		free(jumpMove);
	}
}

void T_appendJumpMovesRecursive(Table* table,TMove* prev,const bool isQueen){
	JumpMove* jumpMoves[isQueen ? 4 : 2];
	const int nMoves= isQueen ? jumpMovesForQueen(prev->playingField,prev->steps[prev->stepC-1],table->white,jumpMoves)
						: jumpMovesForMen(prev->playingField,prev->steps[prev->stepC-1],table->white,jumpMoves);
	//printf("recursive %d\n",nMoves);
	//ORDER IS IMPORTANT (Loop from behind)
	//for(int i=0;i<nMoves;i++){
	for(int i=nMoves-1;i>=0;i--){
		if(i==0){
			//the first one we don't need to clone, just append
			JumpMove* jumpMove=jumpMoves[0];
			movePieceAndMark(prev->playingField,jumpMove);
			TMove_addStep(prev,jumpMove->end);
			TMove_addEnemy(prev,jumpMove->enemy);
			T_appendJumpMovesRecursive(table,prev,isQueen);
			removeMark(prev->playingField,jumpMove);
			free(jumpMove);
		}else{
			//for each 2nd or more jump moves, we need to clone the old one
			JumpMove* jumpMove=jumpMoves[i];
			TMove* new=TMove_clone(prev);
			movePieceAndMark(new->playingField,jumpMove);
			TMove_addStep(new,jumpMove->end);
			TMove_addEnemy(new,jumpMove->enemy);
			T_addMove(table,new,true);
			T_appendJumpMovesRecursive(table,new,isQueen);
			removeMark(new->playingField,jumpMove);
			free(jumpMove);
		}	
	}
}

void T_calculateAllMoves(Table* table){
	const char youMen = table->white ? 'w' : 'b';
	const char youQueen = table->white ? 'W' : 'B';
	for(int row=0;row<8;row++){
		for(int col=0;col<8;col++){
			Point pos;
			pos.row=row;
			pos.col=col;
			if(getVal(pos,table->playingField)==youMen){	
				T_addStepMovesForPiece(table,pos,false);
				T_addJumpMovesForPiece(table,pos,false);
			}else 
			if(getVal(pos,table->playingField)==youQueen){
				T_addStepMovesForPiece(table,pos,true);
				T_addJumpMovesForPiece(table,pos,true);
			}
		}
	}
}



















