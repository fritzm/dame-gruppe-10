#ifndef CONSTI_HEURISTIC_X
#define CONSTI_HEURISTIC_X

#include <stdlib.h>
#include <stdbool.h>


int heuristic_simple(const char playingField[8][8],const bool white);

int heuristic_advanced(char playingField[8][8],const bool white);






#endif //CONSTI_HEURISTIC_X