#ifndef CONSTI_INSTANCE
#define CONSTI_INSTANCE

#include "moves.h"
#include "kvec.h"


//Table move
//more generalized than jumpMove / stepMove (makes no difference between them)
//and also supports multiple jump moves one after another(max 9)
typedef struct TMove TMove;


struct TMove{
	Point start;
	Point steps[9];
	int stepC;
	Point enemies[9];
	int enemyC;
	//Playing field after this TMove has been fully executed
	//set in the end
	char playingField[8][8];
};


typedef struct {
	char playingField[8][8];
	bool white;
	kvec_t(TMove*) stepMoves;
	kvec_t(TMove*) jumpMoves;
	kvec_t(TMove*) possibleMoves;
}Table;


void T_Prepare(Table* table);
void T_Cleanup(Table* table);
	
void T_printStepMoves(Table* table);
void T_printJumpMoves(Table* table);

void T_calculateAllMoves(Table* table);



#endif

