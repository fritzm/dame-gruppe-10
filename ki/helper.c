
#include "helper.h"

char convertRow(const Point p){
	return '0'+8-p.row;
}

char convertCol(const Point p){
	return 'A'+p.col;
}

void printPoint(const Point p){
	printf("[%c,%c]\n",convertCol(p),convertRow(p));
}

bool validate(const Point p){
	if(p.row>=0 && p.row<8 && p.col>=0 && p.col<8){
		return true;
	}
	return false;
}

char getVal(const Point p,const char playingField[8][8]){
	return playingField[p.row][p.col];
}

bool isEnemy(const char val,const bool white){
	if(white){
		return (val=='b' || val=='B');
	}else{
		return (val=='w' || val=='W');
	}
}

bool isEmpty(const char val){
	return (val=='*');
}



