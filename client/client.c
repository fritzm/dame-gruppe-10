#define _POSIX_C_SOURCE_ 2
#define _POSIX_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <netdb.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <getopt.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>


#include "client.h"
#include "performConnection.h"
#include "../ki/simpleKI.h"
#include "../config/config.h"


#define DEFAULT_CONFIG_FILE "client.conf"
#define MY_SHM_SIZE 128

void print_usage(){
	printf("%s\n","Usage: -g <GAME-ID> -p <{1,2}>");
}

//6 chars for "PLAY "
//2 chars for newline and terminator
//up to 8 coordinate points
#define MAX_MOVE_COMMAND_SIZE 6+2+8*3

void concat2(const char *s1, const char *s2,char* result){
    strcpy(result,s1);
    strcat(result,s2);
}

void printPlayingField(char playingField[8][8]){
	printf("   A B C D E F G H  \n");
	printf(" +-----------------+ \n");
	for(int i=0;i<8;i++){
		printf("%d|",8-i);
		for(int j=0;j<8;j++){
			printf(" %c",playingField[i][j]);
		}
		printf(" |%d\n",8-i);
	}
	printf(" +-----------------+ \n");
	printf("   A B C D E F G H  \n");
}

//Parse arguments from command line into options param.
void parse_arguments(int argc,char **argv){
	int option;
	while ((option = getopt (argc, argv, "g:p:k:f:")) != -1)
		switch (option){
			case 'g':
				//printf("g: %s\n",optarg);
				memcpy(cdLineOptions.GAME_ID,optarg,strlen(optarg)+1);
				break;
			case 'p':{
				int val=atoi(optarg)-1;
				if(val<0 || val>1){
					perror("only player 1 or 2 accepted\n");
					exit(EXIT_FAILURE);
				}
				cdLineOptions.PLAYER=val;
			}
				//printf("p: %d\n",cdLineOptions.PLAYER);
				break;
			case 'k':
				cdLineOptions.mKIMode=atoi(optarg);
				//printf("k (ki mode): %d\n",mKIMode);
				break;
			case 'f':
				memcpy(cdLineOptions.mConfigFileName,optarg,strlen(optarg)+1);
				//printf("f: %s \n",mConfigFileName);
				break;
			default:
				printf("default\n"); 
				break;
		}
}

void printArguments(){
	printf("GAME_ID: %s PLAYER: %d\n",cdLineOptions.GAME_ID,cdLineOptions.PLAYER);
}



//Return: the socket fd
int connectSocket(){
	int sockfd;
	struct sockaddr_in server_addr; 
	struct hostent *he;

	if ((he=gethostbyname(config.Hostname)) == NULL) {  /* get the host info */
		perror("Error gethostbyname()\n");
		exit(EXIT_FAILURE);
	}

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("Error socket()");
		exit(EXIT_FAILURE);
	}

	memset(&server_addr, '0', sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(config.Portnumber); 
	//server_addr.sin_addr= *((struct in_addr *)he->h_addr);
	memcpy(&server_addr.sin_addr, he->h_addr_list[0], he->h_length);

	if (connect(sockfd, (struct sockaddr *)&server_addr,sizeof(struct sockaddr)) == -1) {
		perror("Error connect()");
		exit(1);
	}

	printf("Socket ready to use\n");
	return sockfd;	
}

//registered in Parent process (Thinker)
void mySignalHandlerThinker(){
	if(!shmContents->shouldThink){
		printf("ERROR set flag first\n");
		return;
	}
	printf("Flag set. Start thinking\n");
	shmContents->shouldThink=false;
	//printf("I should think now\n");
	printf("Printing playing field in thinker\n");
	printPlayingField(shmContents->playingField);
	
	//ensure buffer is big enough to hold all coordinate points
	char move[MAX_MOVE_COMMAND_SIZE];
	const bool white = (cdLineOptions.PLAYER==0) ? true : false;
	
	calculateNewMove(shmContents->playingField,move,white,cdLineOptions.mKIMode);
	
	char moveCommand[MAX_MOVE_COMMAND_SIZE];
	concat2("PLAY ",move,moveCommand);
	const int size=strlen(moveCommand);
	printf("command: %s %d\n",moveCommand,size);
	//+1 on the size because we want to include the string terminator
	if ((write (fd_pipe[1], moveCommand,size+1)) != size+1) {      
		perror ("Fehler bei write().");
		exit (EXIT_FAILURE);
    }
}

//registered in Parent process (Thinker)
void mySignalHandlerExit(){
	exit(EXIT_SUCCESS);
}

int main(int argc, char * argv[])
{
	//set default params on cdLineOptions
	cdLineOptions.PLAYER=0;
	memcpy(cdLineOptions.GAME_ID,"",sizeof(""));
	cdLineOptions.mKIMode=KI_MODE_ADVANCED;
	//set the defautl config file name
	memcpy(cdLineOptions.mConfigFileName,DEFAULT_CONFIG_FILE,sizeof(DEFAULT_CONFIG_FILE));
	cdLineOptions.PLAYER=0;
	parse_arguments(argc,argv);
	printArguments();

	parseConfig(cdLineOptions.mConfigFileName);
	
	int shmid;
    key_t key = 758600;
	//
	if(pipe(fd_pipe)<0){
		perror("create pipe");
		exit(EXIT_FAILURE);
	}
	
	pid_t pid;
	pid=fork();
	if(pid==0){
		printf("Child (Connector)\n");
		
		if((shmid = shmget(key,sizeof(shmContent_t), IPC_CREAT | 0666)) < 0){
            perror("shmget");
            exit(EXIT_FAILURE);
        }
        shmContents = shmat(shmid, NULL, 0);
		shmContents->shouldThink=false;
		
		//close the writing page
		close(fd_pipe[1]); 
			
		int sockfd=connectSocket();
		performConnection(sockfd);
		//dont forget to close the socket
		close(sockfd);
		//also dont forget the shm
		//key = shmctl(shmid,IPC_RMID,shmContents);
		
		//we are done - send signal to parent
		kill(getppid(),SIGUSR2);
		
	}else{
		printf("Parent (Thinker)\n");
		 if((shmid = shmget(key, sizeof(shmContent_t), IPC_CREAT | 0666)) < 0){
            perror("shmget");
            exit(EXIT_FAILURE);
        }
		shmContents = shmat(shmid, NULL, 0);
		
		//close reading page
		close(fd_pipe[0]);
	
		//keep the process alive for the signal
		while(true){
			//register the signal handler
		    signal(SIGUSR1, mySignalHandlerThinker);
			signal(SIGUSR2, mySignalHandlerExit);
			pause();
		}

	}
	return 0;
}


