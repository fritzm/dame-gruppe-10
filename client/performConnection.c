#define _POSIX_C_SOURCE_ 2
#define _POSIX_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <signal.h>
#include <sys/time.h>
#include <limits.h>
#include <stdbool.h>
//
#include "client.h"

#define READ_BUFFER_SIZE 1024

#define CLIENT_VERSION "VERSION 2.3"
#define MY_GAME_ID "ID 0vf4wle4a5opg" //need make param
#define MY_PLAYER "PLAYER 0"

#define TIMEVAL_WAIT_TIME_US 1000

char buff[READ_BUFFER_SIZE];
char playingField[8][8];


//returns true if string a starts with string b
bool startsWith(const char *a, const char *b){
   return strncmp(a, b, strlen(b))==0;
}

void intToString(int val,char str[2]){
	str[0]='0'+val;
	str[1]='\0';
}

//warning: Dont forget the \0 for strlen
//manually adds the \n for the protocoll
void sendLine(int sockfd, char* line){
	int len=write(sockfd,line,strlen(line));
	write(sockfd,"\n",strlen("\n"));
	if(len<0){
		printf("Cannot write %s",line);
		exit(EXIT_FAILURE);
	}else{
		//printf("Wrote to server: %s\n",line);
	}
}

//reads bytes from socket until a newline (\n) Character is
//found. Returns true on success, false otherwise
//Also adds the string terminator \0 after the \n character
bool receiveLine(int sockfd){
	int count=0;
	while(true){
		char c;
		int len=read(sockfd,&c,1);
		if(len!=1){
			return false;
		}
		buff[count]=c;
		count++;
		if(c=='\n'){ //newline reached
			//dont forget the terminator
			buff[count]='\0';
			//printf("Received from Server: %s",buff); 
			return true;
		}
	}
}

bool receiveBoard(int sockfd){
	if(receiveLine(sockfd)==false){
		perror("Couldnt receive board\n");
		return false;
	};
	if(!startsWith(buff,"+ BOARD")){
		perror("wrong line received, should be BOARD\n");
		return false;
	}
	//char* wAndHeight=&buff[strlen("+ BOARD")];
	//printf("W and H are: %s",wAndHeight);
	//w and h are always 8,8 ( and if not this code would behave undefined)
	//now come all the lines of the playing field,we also parse them into a better understandable format (2D array)
	for(int i=0;i<8;i++){
		if(!receiveLine(sockfd)){
			return false;
		}
		for(int j=0;j<8;j++){
			playingField[i][j]=buff[4+j*2];
		}
	}
	//receive one more line (the 'ENDBOARD')
	if(!receiveLine(sockfd)){
		return false;
	}
	if(!startsWith(buff,"+ ENDBOARD")){
		printf("wrong line received, should be ENDBOARD\n");
		return false;
	}
	return true;
}

bool dataOnPipe(){
	struct timeval tv;
	tv.tv_sec=0;
	tv.tv_usec=TIMEVAL_WAIT_TIME_US;
	fd_set set_pipe;
	FD_ZERO(&set_pipe);
	FD_SET(fd_pipe[0], &set_pipe);
	int retval = select(fd_pipe[0]+1, &set_pipe, NULL, NULL, &tv);
	if (retval == -1){
		perror("select()\n");
	}else if (retval){
		//printf("Data on pipe\n");
		return true;
	}else{
	   //printf("No data on pipe\n");
	}
	return false;
}

bool dataOnSocket(int sockfd){
	struct timeval tv;
	tv.tv_sec=0;
	tv.tv_usec=TIMEVAL_WAIT_TIME_US;
	fd_set set_sock;
	FD_ZERO(&set_sock);
	FD_SET(sockfd, &set_sock);
	int retval = select(sockfd+1, &set_sock, NULL, NULL, &tv);
	if (retval == -1){
		perror("select()\n");
	}else if (retval){
	    //printf("Data on sock\n");
	    return true;
	}else{
	   //printf("No data on sock\n");
	}
	return false;
}


//Spielverlauf
void courseOfPlay(int sockfd){
	while(true){
		if(dataOnPipe()){
			char string[64];
			int len=read (fd_pipe[0], string, PIPE_BUF);         
			printf("Data on pipe:%s  size:%d size:%d\n",string,len,(int)strlen(string));
			sendLine(sockfd,string);
		}
		if(dataOnSocket(sockfd)){
			if(!receiveLine(sockfd)){
				continue;
			};
			if(startsWith(buff,"+ WAIT")){
				sendLine(sockfd,"OKWAIT");
			}else if(startsWith(buff,"+ MOVEOK")){
				printf("MOVE OK !\n");
			}else if(startsWith(buff,"+ MOVE")){
				//get the maximum available time
				//char* time=&buff[strlen("+ MOVE")];
				//printf("Available time %s",time);
				if(!receiveBoard(sockfd)){
					perror("Couldn't receive board\n");
					exit(EXIT_FAILURE);
				}
				sendLine(sockfd,"THINKING");
				//update the shm with the playing field
				//write the flag
				//send the signal to parent
				//printf("Updating shm playing field\n");
				memcpy(shmContents->playingField,playingField,sizeof(playingField));
				//printf("Setting flag for thinker\n");
				shmContents->shouldThink=true;
				//printf("Sending signal SIGUSR1\n");
				if(kill(getppid(),SIGUSR1)!=0){
					perror("Cannot send kill signal\n");
					exit(EXIT_FAILURE);
				}
			}else if(startsWith(buff,"+ GAMEOVER")){
				if(!receiveBoard(sockfd)){
					perror("Couldn't receive board\n");
					exit(EXIT_FAILURE);
				}
				printf("GAME OVER, playing field:\n");
				printPlayingField(playingField);
				receiveLine(sockfd); //player0 won
				receiveLine(sockfd); //player1 won
			}else if(startsWith(buff,"- TIMEOUT")){
				printf("TIMEOUT we didnt send a valid move in time, shit !\n");
			}else if(startsWith(buff,"+ OKTHINK")){
				printf("Okthink\n");
			}else if(startsWith(buff,"+ QUIT")){
				printf("We are done (QUIT)\n");
				return;
			}else if(startsWith(buff,"-")){
				printf("course of play, error message %s",buff);
				return;
			}else{
				printf("course of play, unknown message %s",buff);
				return;
			}
		}
	}
}

//implements the 'prolog' protocoll phase
//when prolog was completed sucesfully, courseOfPlay is called.
//I decided to split these two because the 'else if' dependency became too complex
//but the logik behind them is the same
void performConnection(int sockfd){
	while(true){
		if(receiveLine(sockfd)==false){
			return;
		};
		if(startsWith(buff,"+ MNM Gameserver")){
			//printf("msg1\n");
			sendLine(sockfd,CLIENT_VERSION);
		}else if(startsWith(buff,"+ Client version accepted - please send Game-ID to join\n")){
			//printf("msg2\n");
			char commandBuffer[64];
			concat2("ID ",cdLineOptions.GAME_ID,commandBuffer);
			sendLine(sockfd,commandBuffer);
		}else if(startsWith(buff,"+ PLAYING ")){
			//get the gamekind-name
			char* gamekindName=&buff[strlen("+ PLAYING ")];
			printf("Gamekind name:%s",gamekindName);
			//MS1: exit if game name!="Checkers"
			if(strcmp(gamekindName,"Checkers\n")!=0){
				printf("ERROR- Game not checkers\n");
				exit(EXIT_FAILURE);
			}
			//read one more line- the game name ( it is not important)
			receiveLine(sockfd);
			char commandBuffer[64];
			char asString[2];
			intToString(cdLineOptions.PLAYER,asString);
			concat2("PLAYER ",asString,commandBuffer);
			sendLine(sockfd,commandBuffer);
		}else if(startsWith(buff,"+ YOU ")){
			char* myNumberAndName=&buff[strlen("+ YOU ")];
			printf("I am (number and name) %s",myNumberAndName);
			//receive one more line-it should be 'TOTAL'
			receiveLine(sockfd);
			if(!startsWith(buff,"+ TOTAL")){
				//this should never happen
				printf("EH ?");
			}else{
				char* nPlayers=&buff[strlen("TOTAL")];
				printf("N players: %s",nPlayers);
			}
			//receive one more line-it should contain 'Mitspielernummer', 'Mitspielername' und 'bereit'
			receiveLine(sockfd);
			printf("Other player(s): %s",buff);
		}else if(startsWith(buff,"+ ENDPLAYERS")) {
			//end of Protokoll phase prolog
			printf("ENDPLAYERS reached- End of Prolog. Entering courseOfPlay()\n\n");
			break; //break out of the loop
		}else if(startsWith(buff,"-")){
			printf("performConnection, error message %s",buff);
			return;
		}else{
			printf("performConnection, unknown message %s",buff);
			return;
		}
	}
	//char* bla=malloc(64);
	//bla[0]=' ';
	//when we arrived here, the last message was "+ ENDPLAYERS"
	//and we finished the prolog phase
	courseOfPlay(sockfd);
}


